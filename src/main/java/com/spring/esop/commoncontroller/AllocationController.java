package com.spring.esop.commoncontroller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.spring.esop.constant.AppConstant;
import com.spring.esop.dto.VestingDto;
import com.spring.esop.entity.ExcersieEntity;
import com.spring.esop.service.AllocationService;
import com.spring.esop.service.MonitorizationService;

@RestController
@RequestMapping(value = AppConstant.REQUEST_CONTROLLER)
public class AllocationController {
	
	@Autowired
	private AllocationService allocationService;
	@Autowired
	private MonitorizationService monitorizationService;
	
	@PostMapping(value = AppConstant.INITIATE_ALLOCATION_STATUS)
	public void initiateAllocation()  {
		allocationService.initiateAllocation();
	}	
	
	@PostMapping(value = AppConstant.PREPARE_VESTED_OPTION)
	public void prepareVestedOption(@RequestBody VestingDto dto) {
		monitorizationService.prepareVestedOption(dto);
	}
	@PostMapping(value = AppConstant.APPROVE_ALLOCATION)
	public void updateAllocationStatus(@RequestHeader List<Long>allocatedIdList) {
		allocationService.updateAllocationStatus(allocatedIdList);
	}

	@PostMapping(value = AppConstant.SAVE_EXCERSIE)
	public void saveExerciese(@RequestBody ExcersieEntity entity) {
		monitorizationService.saveExcersie(entity);
	}
}
	
