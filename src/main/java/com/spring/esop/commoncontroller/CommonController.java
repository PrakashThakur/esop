package com.spring.esop.commoncontroller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.spring.esop.constant.AppConstant;
import com.spring.esop.dto.VestingDto;
import com.spring.esop.entity.EmployeeEntity;
import com.spring.esop.entity.ExcersieEntity;
import com.spring.esop.entity.LockIn;
import com.spring.esop.entity.PlanEntity;
import com.spring.esop.repository.PlanRepository;
import com.spring.esop.service.CommonService;

/*Common Controller for Employee and Plan Entity*/

@RestController
@RequestMapping(value = AppConstant.REQUEST_CONTROLLER)
public class CommonController {
	
	@Autowired
	private CommonService commonService;
	
	
	@PostMapping(value = AppConstant.SAVE_PLAN_ENTITY)
	public void savePlan(@RequestBody PlanEntity planEntity) {
		commonService.savePlanDetails(planEntity);
	}
	@PostMapping(value = AppConstant.SAVE_EMPLOYEE_ENTITY)
	public void saveEmployee(@RequestBody EmployeeEntity employeeEntity) {
		commonService.saveEmployeeDetails(employeeEntity);
	}
	@GetMapping(value = AppConstant.FIND_BY_EMPLOYEE_NUMBER)
	public EmployeeEntity findByEmployeeNumber(String employeeNumber){
		return commonService.findByEmployeeNumber(employeeNumber);
	}
	
	@GetMapping(value = AppConstant.FIND_BY_ID)
	public PlanEntity findByPlanId(Long id) {
		return commonService.findByPlanId(id);
	}
	
	@GetMapping(value = AppConstant.FIND_ALL)
	public @ResponseBody PlanEntity findByPlanByYear(String year){
		return commonService.findByPlanByYear(year);
	}
	
	@PostMapping(value = AppConstant.START_MONITORITION)
	public void startMonitorition(@RequestBody VestingDto vestingDto) {
		commonService.startMonitorition(vestingDto);
	}
	@PostMapping(value = AppConstant.SAVE_LOCKIN)
	public  LockIn saveLockIn(@RequestBody LockIn lockIn) {
		return commonService.saveLockIn(lockIn);
	}
	@PutMapping(value = AppConstant.UPDATE_LOCK_STATUS)
	public  void updateLockStatus(@RequestHeader String employeeNumber) {
		commonService.updateLockStatus(employeeNumber);
	}
	
	
	
	
}
