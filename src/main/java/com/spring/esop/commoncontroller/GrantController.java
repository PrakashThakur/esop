package com.spring.esop.commoncontroller;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spring.esop.constant.AppConstant;
import com.spring.esop.dto.GrantDto;
import com.spring.esop.entity.GrantEntity;
import com.spring.esop.service.GrantService;

import ch.qos.logback.classic.Logger;

@RestController
@RequestMapping(value = AppConstant.REQUEST_CONTROLLER)
public class GrantController {
	
	@Autowired
	private GrantService grantService;
	
	@PostMapping(value = AppConstant.SAVE_PROCESS_UPLOAD)
	public void saveGrantProcess(@RequestBody List<GrantDto> grantDto) throws ParseException {
		System.out.println("Executing saveGrantProcess of GrantController class");
		grantService.processUploadGrants(grantDto);
	}
	@PutMapping(value = AppConstant.UPDATE_GRANT_STATUS)
	public void approveGrant(@RequestHeader List<String> grantIdList) {
		grantService.approveGrant(grantIdList);
	}
	@PutMapping(value = AppConstant.UPDATE_ACCEPT_GRANT)
	public void acceptGrant(@RequestHeader List<String>grandIdList) {
		grantService.acceptGrant(grandIdList);
	}
	
	@GetMapping(value = AppConstant.FIND_GRANT_ALLOCATION_STATUS)
	public List<GrantEntity> findByAllocationStatusAndGrantStatus(String grantStatus,Boolean allocationStatus) {
		return grantService.findByAllocationStatusAndGrantStatus(grantStatus, allocationStatus);
	}
}

