package com.spring.esop.constant;

public interface AppConstant {
	
	/*Employee Entity Details*/
	public static String FIND_BY_EMPLOYEE_NUMBER="/findByEmployeeNumber";
	
	/*Plan Entity Details*/
	public static String REQUEST_CONTROLLER="/";
	public static String SAVE_PLAN_ENTITY="/savePlanEntity";
	public static String SAVE_EMPLOYEE_ENTITY="/saveEmployeeEntity";
	public static String FIND_BY_ID="/findById";
	public static String FIND_ALL="/findAll";
	
	/*Grant Entity Details*/
	public static String SAVE_PROCESS_UPLOAD="/saveProcessUpload";
	public static String UPDATE_GRANT_STATUS="/updateGrantStatus";
	public static String UPDATE_ACCEPT_GRANT="/updateAcceptGrant";
	public static String FIND_GRANT_ALLOCATION_STATUS="/findGrandAndAllocationStatus";
	
	/*Allocation Entity Details*/
	public static String INITIATE_ALLOCATION_STATUS="/initiateAllocationStatus";
	public static String START_MONITORITION="/startMonitorition";
	public static String APPROVE_ALLOCATION="/approveAllocation";
	
	/*Vested Entity Details*/
	public static String PREPARE_VESTED_OPTION="/prepareVestedOption";
	
	/*Lock Entity Details*/
	public static String FIND_EMP_NO1="/findByEmpNo";		
	public static String SAVE_LOCKIN="/saveLockIn";
	public static String UPDATE_LOCK_STATUS="/updateLockStatus";
	
	/*Exercise Entity Details*/
	public static String SAVE_EXCERSIE="/saveExcersieDetails";
	
	

}
