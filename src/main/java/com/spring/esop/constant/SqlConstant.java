package com.spring.esop.constant;

public interface SqlConstant {
	/*Sql Table Details*/
	public static String EMPLOYEE_ENTITY="employee_entity";
	public static String GRANT_ENTITY="grant_entity";
	public static String PLANT_ENTITY="plan_entity";
	public static String ALLOCATION_ENTITY="allocation_entity";
	public static String VESTED_OPTIONS="vested_option_entity";
	public static String LOCK_IN="lockIn";
	public static String EXCERSIE="excersie_entity";

}
