package com.spring.esop.dto;


public class GrantDto {
	
	private String grantId;
	
	private String employeeNumber;
	
	private String band;
	
	private Long numberOfGrants;
	
	private Double grantPrice;
	
	private Boolean allocationStatus;
	
	private Long planId;

	public String getGrantId() {
		return grantId;
	}

	public void setGrantId(String grantId) {
		this.grantId = grantId;
	}

	public String getEmployeeNumber() {
		return employeeNumber;
	}

	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}

	public String getBand() {
		return band;
	}

	public void setBand(String band) {
		this.band = band;
	}

	public Long getNumberOfGrants() {
		return numberOfGrants;
	}

	public void setNumberOfGrants(Long numberOfGrants) {
		this.numberOfGrants = numberOfGrants;
	}

	public Double getGrantPrice() {
		return grantPrice;
	}

	public void setGrantPrice(Double grantPrice) {
		this.grantPrice = grantPrice;
	}

	public Boolean getAllocationStatus() {
		return allocationStatus;
	}

	public void setAllocationStatus(Boolean allocationStatus) {
		this.allocationStatus = allocationStatus;
	}

	public Long getPlanId() {
		return planId;
	}

	public void setPlanId(Long planId) {
		this.planId = planId;
	}
	
	

	
	
	
	
	
}