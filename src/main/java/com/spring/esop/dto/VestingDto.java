package com.spring.esop.dto;

import java.util.Date;

public class VestingDto {
	
		public String planYear;
		
		public Double vestingFactor;
		
		public Double fairMarketValue;
		
		private Date monetizationDate;

		public String getPlanYear() {
			return planYear;
		}

		public void setPlanYear(String planYear) {
			this.planYear = planYear;
		}

		public Double getVestingFactor() {
			return vestingFactor;
		}

		public void setVestingFactor(Double vestingFactor) {
			this.vestingFactor = vestingFactor;
		}

		public Double getFairMarketValue() {
			return fairMarketValue;
		}

		public void setFairMarketValue(Double fairMarketValue) {
			this.fairMarketValue = fairMarketValue;
		}

		public Date getMonetizationDate() {
			return monetizationDate;
		}

		public void setMonetizationDate(Date monetizationDate) {
			this.monetizationDate = monetizationDate;
		}
		
		
		
}
