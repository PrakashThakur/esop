package com.spring.esop.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.spring.esop.constant.SqlConstant;

@Entity
@Table(name = SqlConstant.ALLOCATION_ENTITY)
public class AllocationEntity implements Serializable{
	
	@Id
	@GenericGenerator(name = "a_uto",strategy = "increment")
	@GeneratedValue(generator = "a_uto")
	@Column(name = "id")
	private Long id;
	
	@ManyToOne(cascade = CascadeType.MERGE)
	private GrantEntity grant;
	
	@Column(name = "allocation_number")
	private Double allocationNumber;
	
	@Column(name = "allocation_date")
	private String alloationDate;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "allocation_year")
	private String allocationYear;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public GrantEntity getGrant() {
		return grant;
	}

	public void setGrant(GrantEntity grant) {
		this.grant = grant;
	}

	public Double getAllocationNumber() {
		return allocationNumber;
	}

	public void setAllocationNumber(Double allocationNumber) {
		this.allocationNumber = allocationNumber;
	}

	public String getAlloationDate() {
		return alloationDate;
	}

	public void setAlloationDate(String alloationDate) {
		this.alloationDate = alloationDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAllocationYear() {
		return allocationYear;
	}

	public void setAllocationYear(String allocationYear) {
		this.allocationYear = allocationYear;
	}

	@Override
	public String toString() {
		return "AllocationEntity [id=" + id + ", grant=" + grant + ", allocationNumber=" + allocationNumber
				+ ", alloationDate=" + alloationDate + ", status=" + status + ", allocationYear=" + allocationYear
				+ "]";
	}

	
}
