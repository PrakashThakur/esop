package com.spring.esop.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import com.spring.esop.constant.SqlConstant;

@Entity
@Table(name = SqlConstant.EXCERSIE)
public class ExcersieEntity implements Serializable {
	
	@Id
	@GenericGenerator(name = "a_uto",strategy = "increment")
	@GeneratedValue(generator = "a_uto")
	@Column(name = "id")
	private Long id;
	@Column(name = "grantId")
	private String grantId;
	@ManyToOne(cascade = CascadeType.ALL)
	private VestedOptionEntity vestedOption;
	@Column(name = "excersieOptions")
	private Double ExcersieOptions;
	@Column(name = "soldOptions")
	private Double soldOptions;
	@Column(name = "status")
	private String status;
	@Column(name = "created_date")
	private Date createdDate;
	@Column(name = "modified_date")
	private Date modifiedDate;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getGrantId() {
		return grantId;
	}
	public void setGrantId(String grantId) {
		this.grantId = grantId;
	}
	public VestedOptionEntity getVestedOption() {
		return vestedOption;
	}
	public void setVestedOption(VestedOptionEntity vestedOption) {
		this.vestedOption = vestedOption;
	}
	public Double getExcersieOptions() {
		return ExcersieOptions;
	}
	public void setExcersieOptions(Double excersieOptions) {
		ExcersieOptions = excersieOptions;
	}
	public Double getSoldOptions() {
		return soldOptions;
	}
	public void setSoldOptions(Double soldOptions) {
		this.soldOptions = soldOptions;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	@Override
	public String toString() {
		return "ExcersieEntity [id=" + id + ", grantId=" + grantId + ", vestedOption=" + vestedOption
				+ ", ExcersieOptions=" + ExcersieOptions + ", soldOptions=" + soldOptions + ", status=" + status
				+ ", createdDate=" + createdDate + ", modifiedDate=" + modifiedDate + "]";
	}
	
	
}
