package com.spring.esop.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.spring.esop.constant.SqlConstant;

@Entity
@Table(name = SqlConstant.GRANT_ENTITY)
public class GrantEntity implements Serializable {
	
	@Id
	@GenericGenerator(name = "a_uto",strategy = "increment")
	@GeneratedValue(generator = "a_uto")
	@Column(name = "id")
	private Long id;
	
	@ManyToOne(cascade = CascadeType.ALL)
	private PlanEntity plan;
	
	@Column(name = "grantId")
	private String grantId;
	
	@Column(name = "employee_number")
	private String employeeNumber;
	
	@Column(name = "band")
	private String band;
	
	@Column(name = "number_of_grants")
	private Long numberOfGrants;
	
	@Column(name = "grant_price")
	private Double grantPrice;
	
	@Column(name = "grant_status")
	private String grantStatus;
	
	@Column(name = "accetp_date")
	private Date acceptDate;
	
	@Column(name = "granted_date")
	private Date grantedDate;
	
	@Column(name = "accepted")
	private Boolean accepted;
	
	@Column(name = "allocation_staus")
	private Boolean allocationStatus;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PlanEntity getPlan() {
		return plan;
	}

	public void setPlan(PlanEntity plan) {
		this.plan = plan;
	}

	public String getGrantId() {
		return grantId;
	}

	public void setGrantId(String grantId) {
		this.grantId = grantId;
	}

	public String getEmployeeNumber() {
		return employeeNumber;
	}

	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}

	public String getBand() {
		return band;
	}

	public void setBand(String band) {
		this.band = band;
	}

	public Long getNumberOfGrants() {
		return numberOfGrants;
	}

	public void setNumberOfGrants(Long numberOfGrants) {
		this.numberOfGrants = numberOfGrants;
	}

	public Double getGrantPrice() {
		return grantPrice;
	}

	public void setGrantPrice(Double grantPrice) {
		this.grantPrice = grantPrice;
	}

	public String getGrantStatus() {
		return grantStatus;
	}

	public void setGrantStatus(String grantStatus) {
		this.grantStatus = grantStatus;
	}

	public Date getAcceptDate() {
		return acceptDate;
	}

	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}

	public Date getGrantedDate() {
		return grantedDate;
	}

	public void setGrantedDate(Date grantedDate) {
		this.grantedDate = grantedDate;
	}

	public Boolean getAccepted() {
		return accepted;
	}

	public void setAccepted(Boolean accepted) {
		this.accepted = accepted;
	}

	public Boolean getAllocationStatus() {
		return allocationStatus;
	}

	public void setAllocationStatus(Boolean allocationStatus) {
		this.allocationStatus = allocationStatus;
	}

	@Override
	public String toString() {
		return "GrantEntity [id=" + id + ", plan=" + plan + ", grantId=" + grantId + ", employeeNumber="
				+ employeeNumber + ", band=" + band + ", numberOfGrants=" + numberOfGrants + ", grantPrice="
				+ grantPrice + ", grantStatus=" + grantStatus + ", acceptDate=" + acceptDate + ", grantedDate="
				+ grantedDate + ", accepted=" + accepted + ", allocationStatus=" + allocationStatus + "]";
	}

	
	
	
}
