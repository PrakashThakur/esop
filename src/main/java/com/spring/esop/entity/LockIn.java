package com.spring.esop.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.spring.esop.constant.SqlConstant;

@Entity
@Table(name = SqlConstant.LOCK_IN)
public class LockIn implements Serializable {
	
	@Id
	@GenericGenerator(name = "a_uto",strategy = "increment")
	@GeneratedValue(generator = "a_uto")
	@Column(name = "id")
	private Long id;
	
	@Column(name = "employee_number")
	private String employeeNumber;
	
	@Column(name = "lock_status")
	private String lockStatus;
	
	@Column(name = "creating_date")
	private Date creatingDate;
	
	@Column(name = "modified_date")
	private Date modifiedDate;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getEmployeeNumber() {
		return employeeNumber;
	}
	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}
	public String getLockStatus() {
		return lockStatus;
	}
	public void setLockStatus(String lockStatus) {
		this.lockStatus = lockStatus;
	}
	public Date getCreatingDate() {
		return creatingDate;
	}
	public void setCreatingDate(Date creatingDate) {
		this.creatingDate = creatingDate;
	}
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	@Override
	public String toString() {
		return "LockIn [id=" + id + ", employeeNumber=" + employeeNumber + ", lockStatus=" + lockStatus
				+ ", creatingDate=" + creatingDate + ", modifiedDate=" + modifiedDate + "]";
	}
	
	
	
	
}
