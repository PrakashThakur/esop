package com.spring.esop.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.spring.esop.constant.SqlConstant;

@Entity
@Table(name = SqlConstant.PLANT_ENTITY)
public class PlanEntity implements Serializable{
	
	@Id
	@GenericGenerator(name = "a_uto",strategy = "increment")
	@GeneratedValue(generator = "a_uto")
	@Column(name = "id")
	private Long id;
	
	@Column(name = "year")
	private String year;
	
	@Column(name = "start_date")
	private Date startDate;
	
	@Column(name = "actual_date")
	private Date actualEndDate;
	
	@Column(name = "iscurrent_plan_year")
	private boolean isCurrentPlanYear;
	
	@Column(name = "vesting_factory")
	private Double vestingFactory;
	
	@Column(name = "monitization_date")
	private Date monetizationDate;
	
	@Column(name = "fair_market_value")
	private Double fairMarktValue;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getActualEndDate() {
		return actualEndDate;
	}

	public void setActualEndDate(Date actualEndDate) {
		this.actualEndDate = actualEndDate;
	}

	public boolean isCurrentPlanYear() {
		return isCurrentPlanYear;
	}

	public void setCurrentPlanYear(boolean isCurrentPlanYear) {
		this.isCurrentPlanYear = isCurrentPlanYear;
	}

	public Double getVestingFactory() {
		return vestingFactory;
	}

	public void setVestingFactory(Double vestingFactory) {
		this.vestingFactory = vestingFactory;
	}

	public Date getMonetizationDate() {
		return monetizationDate;
	}

	public void setMonetizationDate(Date monetizationDate) {
		this.monetizationDate = monetizationDate;
	}

	public Double getFairMarktValue() {
		return fairMarktValue;
	}

	public void setFairMarktValue(Double fairMarktValue) {
		this.fairMarktValue = fairMarktValue;
	}

	@Override
	public String toString() {
		return "PlanEntity [id=" + id + ", year=" + year + ", startDate=" + startDate + ", actualEndDate="
				+ actualEndDate + ", isCurrentPlanYear=" + isCurrentPlanYear + ", vestingFactory=" + vestingFactory
				+ ", monetizationDate=" + monetizationDate + ", fairMarktValue=" + fairMarktValue + "]";
	}
	
	
	
	
}
