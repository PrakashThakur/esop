package com.spring.esop.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.spring.esop.constant.SqlConstant;
import com.spring.esop.dto.VestingDto;

@Entity
@Table(name = SqlConstant.VESTED_OPTIONS)
public class VestedOptionEntity {

	@Id
	@GenericGenerator(name = "a_uto",strategy = "increment")
	@GeneratedValue(generator = "a_uto")
	@Column(name = "id")
	public Long id;
	
	@Column(name = "grantId")
	public String  grantId;
	
	@Column(name = "vested_option")
	public Double vestedOption;
	
	@Column(name = "created_date")
	public Date createdDate;
	
	@Column(name = "modified_date")
	public Date modifiedDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGrantId() {
		return grantId;
	}

	public void setGrantId(String grantId) {
		this.grantId = grantId;
	}

	public Double getVestedOption() {
		return vestedOption;
	}

	public void setVestedOption(Double vestedOption) {
		this.vestedOption = vestedOption;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	@Override
	public String toString() {
		return "VestedOptionEntity [id=" + id + ", grantId=" + grantId + ", vestedOption=" + vestedOption
				+ ", createdDate=" + createdDate + ", modifiedDate=" + modifiedDate + "]";
	}

	
	
	
}
