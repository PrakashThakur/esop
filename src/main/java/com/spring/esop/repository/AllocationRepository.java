package com.spring.esop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.spring.esop.entity.AllocationEntity;

@Repository
public interface AllocationRepository extends JpaRepository<AllocationEntity,Long> {

}
