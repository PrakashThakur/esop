package com.spring.esop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.spring.esop.entity.ExcersieEntity;

@Repository
public interface ExcersieRepository extends JpaRepository<ExcersieEntity,Long> {

}
