package com.spring.esop.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.spring.esop.entity.GrantEntity;

@Repository
public interface GrantRepository extends JpaRepository<GrantEntity,Long> {

	public GrantEntity findByGrantId(String grantList);
	
	@Query("from GrantEntity where grant_Status=:grantStatus and allocation_Staus=:allocationStatus")
	public List<GrantEntity> findBygrantStatusAndallocatinStatus(String grantStatus,Boolean allocationStatus);

	
}
