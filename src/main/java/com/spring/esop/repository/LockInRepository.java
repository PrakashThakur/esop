package com.spring.esop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.spring.esop.entity.LockIn;

@Repository
public interface LockInRepository extends JpaRepository<LockIn,Long> {

	
	LockIn findByEmployeeNumber(String employeeNumber);
}
