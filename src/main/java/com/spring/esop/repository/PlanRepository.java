package com.spring.esop.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.spring.esop.entity.PlanEntity;

@Repository
public interface PlanRepository extends JpaRepository<PlanEntity,Long> {
				
	@Query("from PlanEntity where year=:year")
	public PlanEntity findByPlanByYear(String year);
}
