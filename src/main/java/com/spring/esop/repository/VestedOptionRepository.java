package com.spring.esop.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.spring.esop.entity.VestedOptionEntity;

@Repository
public interface VestedOptionRepository extends JpaRepository<VestedOptionEntity,Long> {
	
	public VestedOptionEntity findByGrantId(String grantId);

}
