package com.spring.esop.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.spring.esop.dto.Optiondto;
import com.spring.esop.entity.AllocationEntity;
import com.spring.esop.entity.GrantEntity;
import com.spring.esop.entity.PlanEntity;
import com.spring.esop.entity.VestedOptionEntity;
import com.spring.esop.jdbcTemplate.OptionDtoRowMapper;
import com.spring.esop.repository.AllocationRepository;
import com.spring.esop.repository.PlanRepository;
import com.spring.esop.repository.VestedOptionRepository;


@Service
public class AllocationService {

	@Autowired
	private AllocationRepository allocationRepository;
	@Autowired
	private GrantService grantService;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private MonitorizationService monitorizationService;
	@Autowired
	private VestedOptionRepository vestedOptionRepository;
	

//	@Scheduled(cron = "*/30 * * * * *")
//	public void initialize() {
//		new Thread(new Runnable() {
//
//			@Override
//			public void run() {
//				initiateAllocation();
//			}
//		}).start();
//
//	}

	public void initiateAllocation() {

		Calendar now=Calendar.getInstance();

		List<String> list=Arrays.asList("A6","A8","A9","A10");
		
		List<GrantEntity> grantEntity=grantService.findByAllocationStatusAndGrantStatus("granted",false);
		
		
		for (GrantEntity grantEntity2 : grantEntity) {

			List<AllocationEntity> list2=new ArrayList<>();
			
		if(list.contains(grantEntity2.getBand())) {
				
			for(int i=1;i<=5;i++)
			{
				
				AllocationEntity allocationEntity=new AllocationEntity();
				
				allocationEntity.setAlloationDate
				(""+now.get(Calendar.DATE)+"-"+(now.get(Calendar.MONTH)+1)+"-"+(now.get(Calendar.YEAR)+1));

				allocationEntity.setAllocationYear(now.get(Calendar.YEAR)+i+"");	
				allocationEntity.setAllocationNumber((double)grantEntity2.getNumberOfGrants()/5);
				allocationEntity.setGrant(grantEntity2);
				allocationEntity.setStatus("pending");
				list2.add(allocationEntity);
				}
			}
			else
			{
				AllocationEntity allocationEntity=new AllocationEntity();
				
				allocationEntity.setAlloationDate(""+now.get(Calendar.DATE)+"-"+(now.get(Calendar.MONTH)+1)+"-"+(now.get(Calendar.YEAR)+1));
				allocationEntity.setAllocationNumber((double)grantEntity2.getNumberOfGrants());
				allocationEntity.setAllocationYear(" "+(now.get(Calendar.YEAR)+1));
				allocationEntity.setGrant(grantEntity2);
				allocationEntity.setStatus("pending");
				list2.add(allocationEntity);	
			}	
			allocationRepository.saveAll(list2);
			
		}	
	}
	
	public List<Optiondto> findAllocatedAllocationSumByPlan(String planyear)
	{
		
		String sql="""
				select liveproject.plan_entity.id as plan_id, liveproject.grant_entity.grant_id, liveproject.grant_entity.employee_number, sum(liveproject.allocation_entity.allocation_number)
				as sum from liveproject.plan_entity left join liveproject.grant_entity
				on liveproject.grant_entity.plan_id=liveproject.plan_entity.id
				left join liveproject.allocation_entity
				on liveproject.grant_entity.id=liveproject.allocation_entity.grant_id
				where liveproject.allocation_entity.status ="Approved" and liveproject.plan_entity.year="2022" group by liveproject.grant_entity.grant_id;""";
		
		return jdbcTemplate.query(sql, new OptionDtoRowMapper());
	}
	
	public void updateAllocationStatus(List<Long>allocatedIdList) {
		
		List<AllocationEntity>list=new ArrayList<AllocationEntity>();
		
		for(Long allocationList:allocatedIdList)
		{
			Optional<AllocationEntity>aOptional=allocationRepository.findById(allocationList);
			AllocationEntity allocationEntity=aOptional.get();
			if(allocationEntity.getStatus().equalsIgnoreCase("pending"))
			{
				allocationEntity.setStatus("Approved");
			}
			GrantEntity grantEntity=allocationEntity.getGrant();
			
			PlanEntity planEntity=grantEntity.getPlan();
			
			if(monitorizationService.isMonitorizationStarted(planEntity.getId())==true)
			{
				VestedOptionEntity vestedOption=monitorizationService.getVestedOptionByGrantId(grantEntity.getGrantId());
				if(vestedOption!=null)
				{
					vestedOption.setVestedOption(vestedOption.getVestedOption()+planEntity.getVestingFactory()*allocationEntity.getAllocationNumber());
					vestedOptionRepository.save(vestedOption);
				}
				else {
					VestedOptionEntity vestedOption2=new VestedOptionEntity();
					vestedOption2.setCreatedDate(new Date());
					vestedOption2.setModifiedDate(new Date());
					vestedOption2.setGrantId(grantEntity.getGrantId());
					vestedOption2.setVestedOption(planEntity.getVestingFactory()*allocationEntity.getAllocationNumber());
					vestedOptionRepository.save(vestedOption2);	
				}	
			}
			list.add(allocationEntity);
		}
		allocationRepository.saveAll(list);
		
		
	}
		
}


	

