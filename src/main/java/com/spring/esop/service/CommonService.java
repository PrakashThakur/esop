package com.spring.esop.service;


import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.esop.dto.VestingDto;
import com.spring.esop.entity.EmployeeEntity;
import com.spring.esop.entity.LockIn;
import com.spring.esop.entity.PlanEntity;
import com.spring.esop.repository.EmployeeRepository;
import com.spring.esop.repository.LockInRepository;
import com.spring.esop.repository.PlanRepository;

@Service
public class CommonService {
	
	@Autowired
	private PlanRepository planRepository;
	@Autowired
	private EmployeeRepository employeeRepository;
	@Autowired
	private LockInRepository lockInRepository;
	
	
	public void savePlanDetails(PlanEntity planEntity) {
		planRepository.save(planEntity);
	}
	public void saveEmployeeDetails(EmployeeEntity employeeEntity) {
		employeeRepository.save(employeeEntity);
	}
	
	public EmployeeEntity findByEmployeeNumber(String employeeNumber){
		return employeeRepository.findByEmployeeNumber(employeeNumber);
	}
	
	public PlanEntity findByPlanId(Long id) {
		return planRepository.findById(id).get();
	}

	public PlanEntity findByPlanByYear(String year){
		return planRepository.findByPlanByYear(year);
	}
	
	public void startMonitorition(VestingDto vestingDto) {
		
		PlanEntity planEntity=planRepository.findByPlanByYear(vestingDto.getPlanYear());
		
			planEntity.setFairMarktValue(vestingDto.getFairMarketValue());
			planEntity.setVestingFactory(vestingDto.getVestingFactor());
			planEntity.setMonetizationDate(vestingDto.getMonetizationDate());
			planRepository.save(planEntity);
		}
	
	public LockIn saveLockIn(LockIn lockIn) {
		return lockInRepository.save(lockIn);
	}

	
	public void updateLockStatus(String employeeNumber) {
		
		LockIn lockIn = lockInRepository.findByEmployeeNumber(employeeNumber);
		System.out.println(lockIn);
		
			if(lockIn.getLockStatus().equals("open"))
			{
				lockIn.setLockStatus("lock");
				lockIn.setModifiedDate(new Date());
			}
			else 
			{
				lockIn.setLockStatus("open");	
				lockIn.setModifiedDate(new Date());
			}
			lockInRepository.save(lockIn);
		}
	}

	
