package com.spring.esop.service;


import java.text.ParseException;

import java.util.ArrayList;

import java.util.Date;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.spring.esop.dto.GrantDto;
import com.spring.esop.entity.GrantEntity;
import com.spring.esop.entity.PlanEntity;
import com.spring.esop.repository.GrantRepository;
import com.spring.esop.repository.PlanRepository;

@Service
public class GrantService {
	
	@Autowired
	private GrantRepository grantRepository;
	@Autowired
	private CommonService commonService;
	

	public void processUploadGrants(List<GrantDto> grantDtoList) throws ParseException {
		List<GrantEntity> saveGrantEntities=prepareGrant(grantDtoList);
		grantRepository.saveAll(saveGrantEntities);
	}
	
	public List<GrantEntity> prepareGrant(List<GrantDto> grantDto) {
	
		List<GrantEntity> list=new ArrayList<>();
		
		for (GrantDto grantDto2 : grantDto) {
			
			GrantEntity grantEntity=new GrantEntity();
			
			PlanEntity planEntity=commonService.findByPlanId(grantDto2.getPlanId());
			
			grantEntity.setPlan(planEntity);
			grantEntity.setGrantId(grantDto2.getGrantId());
			grantEntity.setEmployeeNumber(grantDto2.getEmployeeNumber());
			grantEntity.setBand(grantDto2.getBand());
			grantEntity.setNumberOfGrants(grantDto2.getNumberOfGrants());
			grantEntity.setGrantPrice(grantDto2.getGrantPrice());
			grantEntity.setAcceptDate(new Date());
			grantEntity.setAccepted(false);
			grantEntity.setGrantedDate(new Date());
			grantEntity.setGrantStatus("pending");
			grantEntity.setAllocationStatus(grantDto2.getAllocationStatus());
			list.add(grantEntity);
		}
		return list;	
	}
	public void approveGrant(List<String> grantIdList) {
		
		List<GrantEntity> list=new ArrayList<>();
	
		for (String grantList: grantIdList) {
			GrantEntity grantEntity=grantRepository.findByGrantId(grantList);
			grantEntity.setGrantStatus("Granted");
			grantEntity.setAllocationStatus(false);
			list.add(grantEntity);
			grantRepository.save(grantEntity);
		}
	}
	public void acceptGrant(List<String>grandIdList) {
		
		for(String grantList:grandIdList) {
			GrantEntity grantEntity=grantRepository.findByGrantId(grantList);
			grantEntity.setAccepted(true);
			grantRepository.save(grantEntity);
		}
	}

	public List<GrantEntity> findByAllocationStatusAndGrantStatus(String grantStatus,Boolean allocationStatus) {
		return grantRepository.findBygrantStatusAndallocatinStatus(grantStatus, allocationStatus);
	}
}

