package com.spring.esop.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.esop.dto.Optiondto;
import com.spring.esop.dto.VestingDto;
import com.spring.esop.entity.ExcersieEntity;
import com.spring.esop.entity.LockIn;
import com.spring.esop.entity.PlanEntity;
import com.spring.esop.entity.VestedOptionEntity;
import com.spring.esop.repository.ExcersieRepository;
import com.spring.esop.repository.LockInRepository;
import com.spring.esop.repository.PlanRepository;
import com.spring.esop.repository.VestedOptionRepository;

@Service
public class MonitorizationService {
	
	@Autowired
	private VestedOptionRepository vestedOptionRepository;
	@Autowired
	private AllocationService allocationService;
	@Autowired
	private PlanRepository planRepository;
	@Autowired
	private ExcersieRepository excersieRepository;
	@Autowired
	private LockInRepository lockInRepository;
	
	public void prepareVestedOption(VestingDto vestingdto )
	{
		
		List<Optiondto> optiondto= allocationService.findAllocatedAllocationSumByPlan(vestingdto.getPlanYear());
		
		List<VestedOptionEntity> vestedOption= new ArrayList<>();
		
		for(Optiondto Optiondto1:optiondto)
		{
			
			VestedOptionEntity option= new VestedOptionEntity();
			
			option.setGrantId(Optiondto1.getGrantId());
			option.setVestedOption(vestingdto.getVestingFactor()*Optiondto1.getAllocatedSum());
			option.setCreatedDate(new Date());
			option.setModifiedDate(new Date());
			vestedOption.add(option);				
		}
		System.out.println(vestedOption);
		vestedOptionRepository.saveAll(vestedOption);
	}	
	public Boolean isMonitorizationStarted(Long planId) {
		
		 PlanEntity planEntity=planRepository.findById(planId).get();
		 
		 if(planEntity.getMonetizationDate()!=null)return true;
		 return false;
	}
	public VestedOptionEntity getVestedOptionByGrantId(String grantId) {
		VestedOptionEntity optionEntity=vestedOptionRepository.findByGrantId(grantId);
		return optionEntity;
	}
	
	
	public void saveExcersie(ExcersieEntity excersieEntity) {
			
		LockIn lockIn=lockInRepository.findById(excersieEntity.getId()).get();
		System.out.println(lockIn);
		
	if(lockIn.getLockStatus().equalsIgnoreCase("open"))
	{
		ExcersieEntity entity=new ExcersieEntity();
		VestedOptionEntity vestedOptionEntity=vestedOptionRepository.findById(excersieEntity.getId()).get();	
		entity.setVestedOption(vestedOptionEntity);
		entity.setGrantId(vestedOptionEntity.getGrantId());
		entity.setExcersieOptions(vestedOptionEntity.getVestedOption()-excersie);
		
		entity.setSoldOptions(setExcersie-50);
		entity.setCreatedDate(new Date());
		entity.setModifiedDate(new Date());
		entity.setStatus("pending");
		
		excersieRepository.save(entity);
		
	}
	else {
		System.out.println("Option have Locked can't be Excersied");
	}
	
		
		
	}
	
	
	
}
	
